#!/usr/bin/env node

import {program, Option, Command, Argument} from "commander";
import {check} from "./commands/check.js";
import {endpoint, password, user} from "./config.js";
import {describe} from "./commands/describe.js";
import {version} from "./commands/version.js";
import {createExternEvent, deleteExternEvent} from "./commands/externevent.js";
import {createExternStatus, deleteExternStatus} from "./commands/externstatus.js";

function main() {
    program
        .name("avantra")
        .option("-e, --endpoint <endpoint>", "URL to the Avantra API", endpoint())
        .option("-u, --user <user>", "User to access the Avantra API", user())
        .option("-d, --debug", "Enable debug output for the Avantra cli", false)
        .addOption(
            new Option(
                "-p, --password <password>",
                "Password to access the API"
            ).default(password())
        )

    program.command('describe [operation]', {hidden: true})
        .description('Show internal SOAP information about the API')
        .action(describe)

    program.command("check")
        .description(`Check the connection to the Avantra API. If the connection can be established and the user
authenticates then the return code will be zero otherwise 90.`
        )
        .action(check);

    program.command("extern-event")
        .description(`Manage extern events for a monitored system.`)
        .command('create')
        .description(`Create an extern event for a monitored system.`)
        .argument("system", `the monitored system - the system ID`)
        .argument("name", "event name")
        .addArgument(new Argument("status", "the status for the event")
            .choices(["CRITICAL", "WARNING", "OK", "UNKNOWN", "DISABLED"]))
        .addArgument(new Argument("message", "the event message to show"))
        .addArgument(new Argument("lifetime",
            `the lifetime of this event in minutes. After that time the event will be removed from
            the monitored system automatically.`).argOptional().default(60))
        .action(createExternEvent)
        .command('delete')
        .description(`Delete an extern event for a monitored system.`)
        .argument("system", `the monitored system - the system ID`)
        .argument("name", "event name")
        .action(deleteExternEvent)

    program.command("extern-status")
        .description(`Manage extern status for a monitored system.`)
        .command('create')
        .description(`Create an extern status for a monitored system.`)
        .argument("system", `the monitored system - the system ID`)
        .argument("name", "event name")
        .addArgument(new Argument("status", "the status for the event")
            .choices(["CRITICAL", "WARNING", "OK", "UNKNOWN", "DISABLED"]))
        .addArgument(new Argument("message", "the event message to show"))
        .action(createExternStatus)
        .command('delete')
        .description(`Delete an extern status for a monitored system.`)
        .argument("system", `the monitored system - the system ID`)
        .argument("name", "event name")
        .action(deleteExternStatus)


    program.command("version")
        .description(`Prints the version information for the Avantra API.`)
        .action(version);

    program.parse();
}

main();
