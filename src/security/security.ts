import soap, {Client, WSSecurity} from "soap";

export interface UserPassword {
    user: string
    password: string
}

export function addSecurity(client: Client, authentication: UserPassword): Client {

    client.setSecurity(
        new WSSecurity(authentication.user, authentication.password, {
            hasNonce: true,
        })
    )

    return client
}