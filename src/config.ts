import config from "config";

export function endpoint(): string {
  return config.get("endpoint");
}

export function user(): string {
  return config.get("user");
}

export function password(): string {
  return config.get("password");
}
