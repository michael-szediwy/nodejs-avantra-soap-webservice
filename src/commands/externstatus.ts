import {Command, CommandOptions} from "commander";
import {Client, createClientAsync} from "soap";
import {fromPromise} from "rxjs/internal/observable/innerFrom";
import {map, switchMap} from "rxjs";
import {addSecurity} from "../security/security.js";
import chalk from 'chalk';

interface CreateExternStatusCommandOptions {
    user: string,
    password: string,
    endpoint: string
    debug: boolean

}

export function createExternStatus(
    system: string,
    name: string,
    status: string,
    message: string,
    option: CommandOptions,
    command: Command
) {
    let options = command.optsWithGlobals<CreateExternStatusCommandOptions>();
    if (options.debug) {
        console.log("Running 'extern-status create' command with following options:")
        console.log(`system=${system}, name=${name}, status=${status}, message=${message}`)
        console.log(options)
    }

    fromPromise<Client>(createClientAsync(options.endpoint))
        .pipe(
            map(client => addSecurity(client, options)),
            switchMap(client => {
                return fromPromise<Array<any>>(client.WriteExternCheckStatusAsync({
                    systemId: system,
                    checkName: name,
                    status: status,
                    result: message
                }))
            }),
        )
        .subscribe({
            next: result => {
                console.log(`Extern status '${name}' for system '${system}': [` + chalk.greenBright("CREATED") + "]")
            },
            error: err => {
                console.error(`Extern status '${name}' for system '${system}': [` + chalk.red("FAILED") + "]")
                console.error(err);
            }
        })
}

export function deleteExternStatus(
    system: string,
    name: string,
    option: CommandOptions,
    command: Command
) {
    let options = command.optsWithGlobals<CreateExternStatusCommandOptions>();
    if (options.debug) {
        console.log("Running 'extern-status delete' command with following options:")
        console.log(`system=${system}, name=${name}`)
        console.log(options)
    }

}