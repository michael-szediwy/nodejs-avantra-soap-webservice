import {Command, CommandOptions} from "commander";
import {Client, createClientAsync} from "soap";
import {fromPromise} from "rxjs/internal/observable/innerFrom";
import {map, switchMap, tap} from "rxjs";
import {addSecurity} from "../security/security.js";
import chalk from 'chalk';
import {MayBe} from "../types.js";

interface DescribeCommandOptions {
    user: string,
    password: string,
    endpoint: string
    debug: boolean
    operation: MayBe<string>
}

function printOperations(result: any, operation: MayBe<string>) {

    if (result.XandriaWebServiceService?.XandriaWebServiceSoap11) {
        const operations = result.XandriaWebServiceService?.XandriaWebServiceSoap11

        if (operation) {
            if (operations[operation]) {
                console.log(operation + ":");
                console.log(operations[operation]);
            } else {
                console.log('No operation found for name: ' + operation)
            }
        } else {
            Object.keys(operations)
                .sort((a, b) => a.localeCompare(b))
                .forEach(value => {
                    console.log(value);
                    // console.log(operations[value]);
                })
        }

    } else {
        console.log('No operations found')
    }

}

export function describe(operation: MayBe<string>, option: CommandOptions, command: Command) {
    let options = command.optsWithGlobals<DescribeCommandOptions>();
    if (options.debug) {
        console.log("Running 'describe' command with following options:")
        if (operation) {
            console.log(`Operation: ${operation}`)
        }
        console.log(options)
    }

    fromPromise<Client>(createClientAsync(options.endpoint))
        .pipe(
            map(client => addSecurity(client, options)),
            map(client => client.describe()),
        )
        .subscribe({
            next: result => printOperations(result, operation),
            error: err => console.error(err)
        })
}