import {Command, CommandOptions} from "commander";
import {Client, createClientAsync} from "soap";
import {fromPromise} from "rxjs/internal/observable/innerFrom";
import {map, switchMap, tap} from "rxjs";
import {addSecurity} from "../security/security.js";
import chalk from 'chalk';

interface CheckCommandOptions {
    user: string,
    password: string,
    endpoint: string
    debug: boolean
}

export function check(option: CommandOptions, command: Command) {
    let options = command.optsWithGlobals<CheckCommandOptions>();
    if (options.debug) {
        console.log("Running check command with following options:")
        console.log(options)
    }

    fromPromise<Client>(createClientAsync(options.endpoint))
        .pipe(
            map(client => addSecurity(client, options)),
            switchMap(client => {
                return fromPromise(client.GetWebServiceInformationAsync(''))
            }),
            // map(value => console.log(value)),
            map(value => 0)
        )
        .subscribe({
            next: code => process.exit(code),
            error: error => {
                if (options.debug) {
                    console.error(error)
                }
                process.exit(90)
            }
        })

}