import {Command, CommandOptions} from "commander";
import {Client, createClientAsync} from "soap";
import {fromPromise} from "rxjs/internal/observable/innerFrom";
import {map, switchMap, tap} from "rxjs";
import {addSecurity} from "../security/security.js";
import chalk from 'chalk';
import {MayBe} from "../types.js";

interface CreateExternEventCommandOptions {
    user: string,
    password: string,
    endpoint: string
    debug: boolean

}

interface VersionResult {
    version: MayBe<string>
}


export function createExternEvent(
    system: string,
    name: string,
    status: string,
    message: string,
    lifetime: MayBe<string>,
    option: CommandOptions,
    command: Command
) {
    let options = command.optsWithGlobals<CreateExternEventCommandOptions>();
    if (options.debug) {
        console.log("Running 'extern-event create' command with following options:")
        console.log(`system=${system}, name=${name}, status=${status}, message=${message}, lifetime=${lifetime}`)
        console.log(options)
    }

    fromPromise<Client>(createClientAsync(options.endpoint))
        .pipe(
            map(client => addSecurity(client, options)),
            switchMap(client => {
                return fromPromise<Array<any>>(client.WriteExternCheckEventAsync({
                    systemId: system,
                    checkName: name,
                    status: status,
                    result: message,
                    expiresInMinutes: lifetime
                }))
            }),
        )
        .subscribe({
            next: result => {
                console.log(`Extern event '${name}' for system '${system}': [` + chalk.greenBright("CREATED") + "]")
            },
            error: err => {
                console.error(`Extern event '${name}' for system '${system}': [` + chalk.red("FAILED") + "]")
                console.error(err);
            }
        })
}

export function deleteExternEvent(
    system: string,
    name: string,
    option: CommandOptions,
    command: Command
) {
    let options = command.optsWithGlobals<CreateExternEventCommandOptions>();
    if (options.debug) {
        console.log("Running 'extern-event delete' command with following options:")
        console.log(`system=${system}, name=${name}`)
        console.log(options)
    }

    fromPromise<Client>(createClientAsync(options.endpoint))
        .pipe(
            map(client => addSecurity(client, options)),
            switchMap(client => {
                return fromPromise<Array<any>>(client.GetWebServiceInformationAsync(''))
                    .pipe(map(value => {
                        return value[0] as VersionResult
                    }))
            }),
        )
        .subscribe({
            next: result => {
                let version = result?.version;
                if (version === '___VERSION___') {
                    console.log(`<dev-version>`)
                } else {
                    console.log(`v${version}`)
                }
            },
            error: err => console.error(err)
        })
}