import {Command, CommandOptions} from "commander";
import {Client, createClientAsync} from "soap";
import {fromPromise} from "rxjs/internal/observable/innerFrom";
import {map, switchMap, tap} from "rxjs";
import {addSecurity} from "../security/security.js";
import chalk from 'chalk';
import {MayBe} from "../types.js";

interface VersionCommandOptions {
    user: string,
    password: string,
    endpoint: string
    debug: boolean
}

interface VersionResult {
    version: MayBe<string>
}


export function version(option: CommandOptions, command: Command) {
    let options = command.optsWithGlobals<VersionCommandOptions>();
    if (options.debug) {
        console.log("Running 'version' command with following options:")
        console.log(options)
    }

    fromPromise<Client>(createClientAsync(options.endpoint))
        .pipe(
            map(client => addSecurity(client, options)),
            switchMap(client => {
                return fromPromise<Array<any>>(client.GetWebServiceInformationAsync(''))
                    .pipe(map(value => {
                        return value[0] as VersionResult
                    }))
            }),
        )
        .subscribe({
            next: result => {
                let version = result?.version;
                if(version === '___VERSION___') {
                    console.log(`<dev-version>`)
                } else {
                    console.log(`v${version}`)
                }
            },
            error: err => console.error(err)
        })
}