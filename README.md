# README #

This is an example on how to access Avantra SOAP WebService using NodeJS. It demonstrates
the access by printing all server checks to the console.

## Prerequisites ##

* Avantra Server has enabled WebServices (Administration -> Settings -> Avantra UI)
* The URL to the Avantra WebFrontend, e.g. https://example.company.com/xn
* User credential on the Avantra Server with the needed permissions.
* NodeJS installation

## Usage ##


## Development ##

### How do I set up the development environment? ###

1. `git clone git@bitbucket.org:michael-szediwy/nodejs-avantra-soap-webservice.git`
2. `cd nodejs-avantra-soap-webservice`
3. `npm install`
4. Configure the endpoint `config/default.json`
   1. endpoint: URL to Avantra UI plus '/ws/xandria_webservice.wsdl', e.g. https://example.company.com/xn/ws/xandria_webservice.wsdl
   2. user: the Avantra username to access the API
   3. password: the user's password as plain text
5. Run it `node .`


### Build ###

```shell
npm run build
```

## Run ##

```shell
node .
```